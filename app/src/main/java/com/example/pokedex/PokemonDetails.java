package com.example.pokedex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marvelclient.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class PokemonDetails extends AppCompatActivity {

    public TextView txtTitulo;
    public ImageView imgThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);

        Bundle b = getIntent().getExtras();
        String json = b.getString("pokemon");

        Gson gson = new Gson();

        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        imgThumbnail = (ImageView) findViewById(R.id.imgThumbnail);

        Pokemon details = gson.fromJson(json, Pokemon.class);

        if (details.getThumbnail() != null) {
            Picasso.get().load(details.getThumbnail())
                    .error(R.drawable.ic_launcher_background).resize(100, 100).into(imgThumbnail);
        } else {

        }

        txtTitulo.setText(details.getName());

    }
}
