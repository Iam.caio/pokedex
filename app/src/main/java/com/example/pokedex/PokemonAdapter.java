package com.example.pokedex;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvelclient.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.MyViewHolder> {

    private ArrayList<Pokemon> mDataset;
    private View.OnClickListener listener;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitulo;
        public TextView txtSubtitulo;
        public ImageView imgThumbnail;


        public MyViewHolder(View view) {
            super(view);
            txtTitulo = (TextView)view.findViewById(R.id.txtTitulo);
            imgThumbnail = (ImageView)view.findViewById(R.id.imgThumbnail);
        }
    }

    public PokemonAdapter(ArrayList<Pokemon> myDataset, View.OnClickListener mlistener) {
        mDataset = myDataset;
        listener = mlistener;
    }

    @Override
    public PokemonAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pokemon pokemon = mDataset.get(position);

        holder.itemView.setTag(position);

        holder.txtTitulo.setText(pokemon.getName());

        holder.itemView.setOnClickListener(listener);

        System.out.println(pokemon.getThumbnail());
        if (pokemon.getThumbnail() != null) {
            Picasso.get().load(pokemon.getThumbnail())
                    .error(R.drawable.ic_launcher_background).resize(100, 100).into(holder.imgThumbnail);
        } else {

        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
