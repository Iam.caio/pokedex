package com.example.pokedex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.marvelclient.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RequestQueue queue;
    private RequestQueue queueSub;
    private ArrayList<Pokemon> pokemons;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pokemons = new ArrayList<>();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = v.getTag().toString();
                Pokemon pokemonDetails = pokemons.get(Integer.parseInt(tag));
                Gson gson = new Gson();

                Intent intent = new Intent(MainActivity.this, PokemonDetails.class);

                intent.putExtra("pokemon", gson.toJson(pokemonDetails));
                startActivity(intent);
                finish();
                return;
            }
        };

        mAdapter = new PokemonAdapter(pokemons, listener);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        String url = "https://pokeapi.co/api/v2/pokemon/";

        queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            JSONArray results = response.getJSONArray("results");
//                            JSONObject data = response.getJSONObject("data");
//                            JSONArray results = data.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject object = results.getJSONObject(i);
                                final Pokemon pokemon = new Pokemon();
                                pokemon.setName(object.getString("name"));

                                String urlSub = object.getString("url");

                                pokemon.setUrl(urlSub);

                                JsonObjectRequest jsonObjectRequestSub = new JsonObjectRequest(Request.Method.GET,
                                        urlSub, null, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            System.out.println("OPA");
                                            System.out.println(response.toString());
                                            JSONObject objPokemon = response.getJSONObject("sprites");
                                            String thumbnail = objPokemon.getString("front_default");

                                            pokemon.setThumbnail(thumbnail);

                                        } catch (JSONException ex) {

                                        }
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("VOLLEY", error.getMessage());

                                    }
                                });

                                queue.add(jsonObjectRequestSub);

//                                character.setName(object.getString("name"));
//                                character.setDescription(object.getString("description"));
//                                JSONObject thumbnail = object.getJSONObject("thumbnail");
//                                character.setThumbnail(thumbnail.getString("path") + "." + thumbnail.getString("extension"));
                                pokemons.add(pokemon);
                            }
                        } catch (JSONException ex) {
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.getMessage());

                    }
                });

        queue.add(jsonObjectRequest);


    }
}
